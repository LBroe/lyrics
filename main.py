import time
from pdf2image import convert_from_path
import tempfile
import tkinter
from PIL import Image, ImageTk
import os
import sys
import threading
import argparse
from datetime import datetime

# The time in ms after which to skip to the next/last PDF
LONG_PRESS_DELAY_MS = 600
TIMER_SIZE = 44
WARNING_SIZE = 55
OVERLAP = 500

parser = argparse.ArgumentParser(description="Zeigt mehrere PDF-Dokumente hintereinander im Vollbildmodus an. "
                                             "Kann durch Pfeiltasten gesteuert werden: Kurz drücken geht "
                                             "eine Seite vor oder zurück, lange drücken springt zum nächsten oder "
                                             "letzten PDF-Dokument. Zum Beenden Escape drücken.")

parser.add_argument("directory", help="Der Ordner mit den PDF-Dateien")
parser.add_argument("--setlist", help="txt-Datei mit relativen Pfaden von directory aus zu den PDF-Dateien "
                                      "in der richtigen Reihenfolge. "
                                      "Ist keine Setlist angegeben, werden alle Dateien in alphabetischer Reihenfolge "
                                      "angezeigt. Soll bei --name ein anderer Name als der Dateiname "
                                      "angezeigt werden, muss das in der Setlist mit \"<Pfad> | <Name>\" "
                                      "angegeben werden.", default=None)
parser.add_argument("--timer", help="Zeigt einen Timer an.", action="store_true")
parser.add_argument("--clock", help="Zeigt eine Uhr an.", action="store_true")
parser.add_argument("--songnumber", help="Zeigt die Nummer des aktuellen Lieds an.", action="store_true")
parser.add_argument("--pagenumber", help="Zeigt die Nummer der aktuellen Seite des Lieds an.", action="store_true")
parser.add_argument("--name", help="Zeigt den Namen des aktuellen Lieds an.", action="store_true")
parser.add_argument("--showfull", help="Zeigt die ganze aktuelle Seite an, anstatt zu scrollen.", action="store_true")


if len(sys.argv) == 1:  # No directory is given, print help and exit
    parser.print_help(sys.stderr)
    sys.exit(1)

args = parser.parse_args()

if not os.path.isdir(args.directory):
    print("Not a directory:", args.directory)
    sys.exit(1)

if args.setlist is not None and (not os.path.exists(args.setlist) or os.path.isdir(args.setlist)):
    print("Not a file:", args.setlist)
    sys.exit(1)

def load_pdfs(l, filenames=None):
    """
    For loading the PDFs in the background.
    """

    if args.setlist is None:
        all_filenames = [f for f in os.listdir(args.directory) if f.endswith(".pdf")]
        all_filenames.sort()
        alternate_names = [None] * len(all_filenames)
    else:
        with open(args.setlist, "r") as dat:
            all_filenames = dat.read().split("\n")

        unsplit_filenames = [f.strip() for f in all_filenames if not f.strip().startswith("#") and f.strip() != ""]
        all_filenames = []
        alternate_names = []
        for n in unsplit_filenames:
            if "|" in n:
                all_filenames.append(n.split("|")[0].strip())
                alternate_names.append(n.split("|")[1].strip())
            else:
                all_filenames.append(n.strip())
                alternate_names.append(None)

    if filenames is not None:
        for i in range(len(all_filenames)):
            filenames.append(all_filenames[i] if alternate_names[i] is None else alternate_names[i])

    for f in all_filenames:
        print(len(l) + 1, "/", len(all_filenames))
        with tempfile.TemporaryDirectory() as path:
            images_from_path = convert_from_path(os.path.join(args.directory, f), output_folder=path)

        l.append([[i,None] for i in images_from_path])

        if not getattr(threading.currentThread(), "do_run", True):
            print("Aborting")
            break

    if len(l) == 0:
        raise RuntimeError("No PDFs in directory/setlist")


class FullscreenImageSlideshow:
    """
    Shows an array of arrays of images as a fullscreen slide show, arrays can be skipped by long-pressing.
    """
    def __init__(self, array, load_thread=None, all_filenames=None):
        self.current_pdf = 0
        self.current_page = 0
        self.offset = 0
        self.load_thread = load_thread
        self.all_filenames = all_filenames
        self.array = array
        self.current_image = None  # So the image is not garbage-collected
        self.left_await = None
        self.right_await = None
        self.fullscreen = True

        self.root = tkinter.Tk()
        self.w, self.h = self.root.winfo_screenwidth(), self.root.winfo_screenheight()
        self.root.geometry("%dx%d+100+100" % (self.w, self.h))
        self.root.configure(background="black")
        self.content = tkinter.Label(self.root, width=self.w, height=self.h)
        self.content.configure(background='black')
        self.content.bind("<Escape>", self.escape)
        self.content.bind("<Control-c>", self.escape)
        self.content.bind("<KeyPress-Left>", self.left)
        self.content.bind("<KeyPress-Right>", self.right)
        self.content.bind("<KeyRelease-Left>", self.left_release)
        self.content.bind("<KeyRelease-Right>", self.right_release)
        def configured(e):
            print(self.root.winfo_width())
            if self.root.winfo_width() != self.w or self.root.winfo_height() != self.h:
                print("Hallo!")
                self.offset = 0
                bef = time.time()
                self.refresh_image()
                print(time.time() - bef)
            self.root.update_idletasks()
        # Made weird slowdowns and isn't really needed
        # self.root.bind("<Configure>", configured)
        self.content.bind("f", self.f)

        if args.timer:
            self.starttime = time.time()
            self.timer = tkinter.Label(self.root, text="Welcome!", fg="white", bg="black")
            self.timer.config(font=("Courier", TIMER_SIZE))
            self.timer.place(relx=0, rely=0)

        if args.clock:
            self.clock = tkinter.Label(self.root, text="What's up?", fg="white", bg="black")
            self.clock.config(font=("Courier", TIMER_SIZE))
            self.clock.place(anchor="ne", relx=1, rely=0)

        if args.timer or args.clock:
            self.root.after(1000, self.timer_step)

        if args.songnumber:
            self.songcounter = tkinter.Label(self.root, text="1/{}".format(len(array)), fg="white", bg="black")
            self.songcounter.config(font=("Courier", TIMER_SIZE))
            self.songcounter.place(anchor="se", relx=1, rely=1)

        if args.pagenumber:
            self.pagecounter = tkinter.Label(self.root, text="1/{}".format(len(array[0])), fg="white", bg="black")
            self.pagecounter.config(font=("Courier", TIMER_SIZE))
            self.pagecounter.place(anchor="s", relx=0.5, rely=1)

        if args.name and self.all_filenames is not None:
            self.songname = tkinter.Label(self.root, text="", fg="white", bg="black")
            self.songname.config(font=("Courier", TIMER_SIZE))
            self.songname.place(anchor="sw", relx=0, rely=1)

        self.warning = tkinter.Label(self.root, text="", fg="red", bg="white")
        self.warning.config(font=("Courier", WARNING_SIZE))

        self.refresh_image()

        self.root.attributes("-fullscreen", self.fullscreen)
        self.root.wm_attributes("-topmost", 1)
        self.root.after(200, lambda: self.content.focus_set())
        if args.showfull:
            self.content.place(anchor="n", relx=0.5, rely=0)
        else:
            self.content.place(x=0, y=0)
        self.root.mainloop()

    def refresh_image(self, image=None, bottom=False):
        if self.root.winfo_width() > 10 and self.root.winfo_height() > 10:
            self.w, self.h = self.root.winfo_width(), self.root.winfo_height()
        if image is None:
            image = self.array[self.current_pdf][self.current_page][0]
        img_width, img_height = image.size
        if args.showfull:
            ratio = min(self.w / img_width, self.h / img_height)
        else:
            ratio = self.w/img_width
        img_width = int(img_width * ratio)
        img_height = int(img_height * ratio)
        already_resized = self.array[self.current_pdf][self.current_page][1]
        if already_resized and already_resized.width() == img_width and already_resized.height() == img_height:
            image = already_resized
        else:
            image = image.resize((img_width, img_height), Image.BICUBIC)
            image = ImageTk.PhotoImage(image)
            self.array[self.current_pdf][self.current_page][1] = image

        self.content["image"] = image
        #self.content.place_forget()
        if bottom:
            self.offset = img_height - self.h
        self.content.configure(width=img_width, height=img_height)
        if args.showfull:
            self.content.place(anchor="n", relx=0.5, rely=0)
        else:
            self.content.place(x=0, y=-self.offset)
        self.current_image = image
        if args.songnumber:
            if self.all_filenames is not None:
                song_number = len(self.all_filenames)
            elif self.load_thread:
                if self.load_thread.is_alive():
                    song_number = str(len(self.array)) + "?"
                else:
                    song_number = len(self.array)
            else:
                song_number = len(self.array)
            self.songcounter["text"] = "{}/{}".format(self.current_pdf + 1, song_number)

        if args.pagenumber:
            self.pagecounter["text"] = "{}/{}".format(self.current_page + 1, len(self.array[self.current_pdf]))

        if args.name and self.all_filenames is not None:
            self.songname["text"] = self.all_filenames[self.current_pdf].split("/")[-1].replace(".pdf", "")

    def escape(self, e):
        self.root.withdraw()
        self.root.quit()

    def left(self, e):
        bottom = False
        if self.offset == 0:
            self.current_page -= 1
            bottom = True
        else:
            self.offset = max(0, self.offset - self.h + OVERLAP)
        if self.current_page < 0:
            if self.current_pdf == 0:
                self.show_warning("Erstes Lied")
                self.current_page = 0
                self.offset = 0
                bottom = False
            else:
                self.current_pdf -= 1
                self.current_page = len(self.array[self.current_pdf]) - 1
        self.refresh_image(bottom=bottom)
        self.left_await = self.root.after(LONG_PRESS_DELAY_MS, self.skip_back)

    def left_release(self, e):
        if self.left_await is not None:
            self.root.after_cancel(self.left_await)

    def skip_back(self):
        self.current_page = 0
        self.offset = 0
        self.refresh_image()

    def right(self, e):
        if self.current_image.height() - self.offset - self.h != 0:
            self.offset = min(self.current_image.height() - self.h, self.offset + self.h - OVERLAP)
        else:
            self.current_page += 1
            if self.current_page == len(self.array[self.current_pdf]):
                if self.current_pdf == len(self.array) - 1:
                    if self.load_thread and self.load_thread.is_alive():
                        self.show_warning("Lied lädt...")
                    else:
                        self.show_warning("Letztes Lied")
                    self.current_page = len(self.array[self.current_pdf]) - 1
                else:
                    self.current_pdf += 1
                    self.current_page = 0
                    self.offset = 0
            else:
                self.offset = 0
        self.refresh_image()
        self.right_await = self.root.after(LONG_PRESS_DELAY_MS, self.skip_ahead)

    def right_release(self, e):
        if self.right_await is not None:
            self.root.after_cancel(self.right_await)

    def skip_ahead(self):
        if self.current_page != 0 or self.offset != 0:
            if self.current_pdf != len(self.array) - 1:
                self.offset = 0
                self.current_page = 0
                self.current_pdf = self.current_pdf + 1
            else:
                if self.load_thread and self.load_thread.is_alive():
                    self.show_warning("Lied lädt...")
                else:
                    self.show_warning("Letztes Lied")
            self.refresh_image()

    def f(self, e):
        self.fullscreen = not self.fullscreen
        self.root.attributes("-fullscreen", self.fullscreen)

    def timer_step(self):
        if args.clock:
            now = datetime.now()
            self.clock["text"] = "{:02}:{:02}".format(now.hour, now.minute)
        if args.timer:
            elapsed = time.time() - self.starttime
            self.timer["text"] = "{:02}:{:02}:{:02}".format(int(elapsed//60**2), int(elapsed//60), int(elapsed % 60))

        self.root.after(1000, self.timer_step)

    def show_warning(self, text):
        self.warning["text"] = text
        self.warning.place(anchor="center", relx=0.5, rely=0.5)
        self.root.after(2000, self.warning.place_forget)

if __name__ == "__main__":
    load_thread = None
    try:
        os.system('xset r off')  # So the key is not repeated when holding
        image_list = []
        filenames = []
        load_thread = threading.Thread(target=load_pdfs, args=(image_list,filenames))
        load_thread.start()
        time.sleep(0.2)
        while len(image_list) == 0:
            if not load_thread.is_alive():
                print("Error while reading PDFs")
                sys.exit(1)
            print("Waiting until first PDF is converted")
            time.sleep(0.5)
        shower = FullscreenImageSlideshow(image_list, load_thread, filenames)
    finally:
        print("Bye")
        os.system('xset r on')
        if load_thread:
            load_thread.do_run = False
